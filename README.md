Ansible Role: Redis Insight
===========================
This role to install redis insight binary.

Salient Features
----------------
- Inspect your Redis data, monitor health, and perform runtime server configuration with a browser-based management interface for your Redis deployment.

Supported OS
------------
   * Linux distribution

Directory Layout
----------------
```
├── README.md
├── meta
│   └── main.yml
├── tasks
│   ├── install.yml
│   ├── main.yml
└── vars
    └── main.yml

```

Role Variables
--------------

|**Variables**| **Default Values**| **Description**| **Type**|
|----------|---------|---------------|-----------|
| binary_dir | /usr/local/bin | binary directory | Mandatory |
| rdbtools_url | url of redis insight | redis insight url | Mandatory |

Example Playbook
----------------
```
---
- name: It will automate Redis insight setup
  hosts: server
  become: true
  roles:
    - role: osm_redis_insight
...

$  ansible-playbook site.yml -i inventory

```

Inventory
----------
An inventory should look like this:-
```
[server]                 
192.xxx.x.xxx    ansible_user=ubuntu 
```

References
----------
- **[Guide followed 1](https://rdbtools.com/docs/install/linux/)**

Author Information
------------------
```
Name: Ashutosh Mishra
MailID: ashutosh.mishra@opstree.com
```

